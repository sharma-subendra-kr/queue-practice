package com.example.demo.models;

import lombok.Data;

@Data
public class RequestCorrelation {
	public static final String CORRELATION_KEY = "correlation-id";
	private String correlationId;
}
